These are the instructions to run the program, they can also be found in the report from blackboard in section 3| User Guide.

1. Download the folder Molloy_D_24234273_CW2 from the submission dropbox to get the report, the files from here (https://bitbucket.org/davidmolloy10/cw2/) should then be downloaded/copied to your local machine. If you need to copy and paste the code into a file rather than download it, ensure you keep the naming the same (vending_machine.js) and save it to the location of your choice

2. Open the console and navigate to the directory where you have saved the files from step 1 to, this can be done by using the cd command and entering the file path and pressing the enter key.

3. For the program to function correctly, the readlineSync module is required. This can be downloaded by entering npm install --save readline-sync into the console. If you have copied the files from the bitbucket repository, this will tell you that it is already installed but it is best to run the code to be sure!

4. The final step is to enter the command "js vending_machine.js" into the console and press enter, this will start up the program and allow the user to start using it.