//Imports the readline module to be able to take user input
readline = require('readline-sync');

//Display function simple shows a greeting message when called
function display() {
    console.log('-============================-');
    console.log('|Welcome to the Vending Machine|');
    console.log('-============================-');
}

//dateTimeToday Function displays the date and time when called
function dateTimeToday() {
    var today = new Date(); //Creates a new variable called today which allows access to the date/time features needed
    var date = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear(); //Date variable gets the current date, month and full year (the +1 shows it in normal form)
    var time = today.getHours() + ':' + today.getMinutes(); //Time variable gets the current hour and minutes
    console.log("Today's date is:" + date + ', and the time currently is: ' + time); //Logs the date and time.
}


//options function shows the user what available options there is, takes their input and will then call a specific function based on that input
function options() {
    console.log('-============================-');
    console.log('1. View the available items');
    console.log('2. Add Credit to your balance');
    console.log('3. Remove Credit from your balance');
    console.log('4. View your current balance');
    console.log('5. Select an Item');
    console.log('6. Leave the vending machine')
    console.log('-============================-');
    
    //Takes user input using the readline.question function
    var input = readline.question("\n" + 'Please enter the ID number of the option you would like to pick: '); 

    //If statement to dected what number the user input 
    if(input == 1) {
        console.log("\n" +'You have selected Option 1: View the available items' + "\n"); //Prints the option the user selected
        listItems(); //Calls the listItems function
    } else if(input == 2) {
        console.log("\n" +'You have selected Option 2: Add Credit to your account' + "\n"); //Prints the option the user selected
        addCredit(); //Calls the add credit function
    } else if(input == 3) {
        console.log("\n" +'You have selected Option 3: Get a refund on your balance' + "\n"); //Prints the option the user selected
        removeCredit() //Calls the removeCredit function
    } else if(input == 4) {
        console.log("\n" +'You have selected Option 4: View your current balance' + "\n"); //Prints the option the user selected
        viewCredit(); //Calls the viewCredit function
    } else if(input == 5) {
        console.log("\n" +'You have selected Option 5: Select an Item' + "\n"); //Prints the option the user selected
        selection(); //Calls the selection function
    } else if(input == 6) {
        console.log('-==========================================-');
        console.log('|Thank you for using the vending machine!|');
        console.log('-==========================================-');
    } else { //Else condition for if the user input doesn't match an option
        console.log("\n" + 'That was not an option, please try again' + "\n"); //Tells the user that their input was not an option 
        options(); //Re-calls the options function
    }
}

//List items function to list all the available items in the vending machine
function listItems() {
    console.log('-============================-');
    console.log('The available items are: ' + "\n");
    console.log(dairyMilk); //Prints the diaryMilk object
    console.log(galaxy) //Prints the galaxy object
    console.log(readySaltedCrisps); //Prints the readySaltedCrisps object
    console.log(dietCoke); //Prints the dietCoke object
    console.log(pepsi); //Prints the pepsi option
    console.log(coke); //Prints the coke option
    console.log('-============================-');
    options();
}

//Var balance creates the balance vairable and sets it to 0
var balance = 0;

//addCredit function to add credit to the balance of the user
function addCredit() {
    console.log('To add balance to your account please enter the amount you wish to add' + "\n"); //Asks the user to input the amount they wish to add.
    var credit = readline.question('Enter the amount: '); //Creates a credit variable to take and store the users input
    balance = parseFloat(credit) + balance;  //Set the balance variable to the parsedFloat of the credit input and adds it to the balance variable.
    console.log('Your new balance is now: ' + '£' + balance.toFixed(2)); //Tells the user what their balance is to 2 decimal places.
    console.log("\n"); //Takes a new line
    options(); //Calls the options function for the user to make further choices.
    return balance; //Returns the balance variable to update it globally.

}


//removeCredit function allows the user to refund the credit from their account
function removeCredit() {
    balance = 0; //set the balance back to 0
    console.log('Your new balance is now: ' + '£' + balance.toFixed(2) + ', the refund has been completed.'); //Prints the balance to show the user it has been refunded.
    console.log("\n"); //Take a new line
    options(); //Call for the options function
    return balance; //Return the balance variable to update it globally back to 0
}

//viewCredit function allows the suer to see their current balance
function viewCredit(credit) {
    console.log('Your current balance is: ' + '£' + balance.toFixed(2)); //Prints tyhe balance variable to 2 decimals and some text to let the user know this is the current balance
    console.log("\n"); //Takes a new line
    options(); //Calls for the options function
}
 

//item function is the constructor class for the vending machine products
function item() {
    this.id =  0; //Products have an ID
    this.name = ''; //Products have a name
    this.price = 0.00; //Products have a price
}

var dairyMilk = new item(); //Makes a new item called dairyMilk
dairyMilk.id = 1; //Gives the item the ID of 1
dairyMilk.name = 'Dairy Milk'; //Gives it a readable name "Dairy Milk"
dairyMilk.price = 0.70; //Gives the item a price

var galaxy = new item(); //Makes a new item called galaxy
galaxy.id = 2; //Gives the item the ID of 2
galaxy.name = 'Galaxy'; //Gives it a readable name "Galaxy"
galaxy.price = 0.70; //Gives the item a price

var readySaltedCrisps = new item(); //Makes a new item called readySaltedCrisps
readySaltedCrisps.id = 3; //Gives the item the ID of 3
readySaltedCrisps.name = 'Ready Salted Crisps'; //Gives it a readable name "Ready Salted Crisps"
readySaltedCrisps.price = 1.00; //Gives the item a price

var dietCoke = new item(); //Makes a new item called dietCoke
dietCoke.id = 4; //Gives the item the ID of 4
dietCoke.name = 'Diet Coke'; //Gives it a readable name "Diet Coke"
dietCoke.price  = 1.20; //Gives the item a price

var pepsi = new item(); //Makes a new item called pepsi
pepsi.id = 5; //Gives the item the ID of 5
pepsi.name = 'Pepsi'; //Gives it a readable name "Pepsi"
pepsi.price  = 1.50; //Gives the item a price

var coke = new item(); //Makes a new item called coke
coke.id = 6; //Gives the item the ID of 6
coke.name = 'Coke'; //Gives it a readable name "Coke"
coke.price  = 1.70; //Gives the item a price

//selection function will detect what the ID the user inputed and respond with the according action below.
function selection() {
    var select = readline.question("Please enter the ID number of the item you want to purchase: "); //Uses the readline.question function to get the user to input the ID of the item they would like
    if(select == dairyMilk.id) { //If the id from select matches dairyMilk.id this block will run
        console.log("You have selected: " + dairyMilk.name); //Shows the user the item they have selected
        if(balance >= dairyMilk.price) { //If statement for check if the users balance is more than or equal to the price of the item dairy milk
            balance = balance - dairyMilk.price; //Removes the item price from the current balance and sets the balance variable to that number
            console.log('Thank you for your purchase' + "\n" +'Your new balance after the purchase is: ' + '£' + balance.toFixed(2) + "\n"); //Prints the new balance after the purchase
            options(); //Calls for the option function
            return balance; //Returns the balance variable to update it globally
        } else { //Else condition if the users balance is lower than the item price
            console.log('You did not have enough credit, your balance is ' + '£' + balance.toFixed(2) + ' and the item costs: ' + '£' + dairyMilk.price); //Tells the user they did not have enough balance and informs them of their balance and the items cost
            options(); //Calls the options function
        }
    } else if(select == galaxy.id) { //ElseIf for the next ID
        console.log("You have selected: " + galaxy.name); //Shows the user the item they have selected
        if(balance >= galaxy.price) { 
            balance = balance - galaxy.price; //Removes the item price from the current balance and sets the balance variable to that number
            console.log('Thank you for your purchase' + "\n" +'Your new balance after the purchase is: ' + '£' + balance.toFixed(2) + "\n"); //Prints the new balance after the purchase
            options(); //Calls the option function
            return balance; //Returns the balance variable to update it globally
        } else { //Else condition if the users balance is lower than the item price
            console.log('You did not have enough credit, your balance is ' + '£' + balance.toFixed(2) + ' and the item costs: ' + '£' + galaxy.price); //Tells the user they did not have enough balance and informs them of their balance and the items cost
            options(); //Calls the option function
        }
    } else if(select == readySaltedCrisps.id) { //ElseIf for the next ID
        console.log("You have selected: " + readySaltedCrisps.name); //Shows the user the item they have selected
        if(balance >= readySaltedCrisps.price) {
            balance = balance - readySaltedCrisps.price; //Removes the item price from the current balance and sets the balance variable to that number
            console.log('Thank you for your purchase' + "\n" +'Your new balance after the purchase is: ' + '£' + balance.toFixed(2) + "\n"); //Prints the new balance after the purchase
            options(); //Calls the option function
            return balance; //Returns the balance variable to update it globally
        } else { //Else condition if the users balance is lower than the item price
            console.log('You did not have enough credit, your balance is ' + '£' + balance.toFixed(2) + ' and the item costs: ' + '£' + readySaltedCrisps.price); //Tells the user they did not have enough balance and informs them of their balance and the items cost
            options(); //Calls the option function
        }
    } else if(select == dietCoke.id) { //ElseIf for the next ID
        console.log("You have selected: " + dietCoke.name); //Shows the user the item they have selected
        if(balance >= dietCoke.price) {
            balance = balance - dietCoke.price; //Removes the item price from the current balance and sets the balance variable to that number
            console.log('Thank you for your purchase' + "\n" +'Your new balance after the purchase is: ' + '£' + balance.toFixed(2) + "\n"); //Prints the new balance after the purchase
            options(); //Calls the option function
            return balance; //Returns the balance variable to update it globally
        } else { //Else condition if the users balance is lower than the item price
            console.log('You did not have enough credit, your balance is ' + '£' + balance.toFixed(2) + ' and the item costs: ' + '£' + dietCoke.price); //Tells the user they did not have enough balance and informs them of their balance and the items cost
            options(); //Calls the option function
        }
    } else if(select == pepsi.id) { //ElseIf for the next ID
        console.log("You have selected: " + pepsi.name); //Shows the user the item they have selected
        if(balance >= pepsi.price) {
            balance = balance - pepsi.price; //Removes the item price from the current balance and sets the balance variable to that number 
            console.log('Thank you for your purchase' + "\n" +'Your new balance after the purchase is: ' + '£' + balance.toFixed(2) + "\n"); //Prints the new balance after the purchase
            options(); //Calls the option function
            return balance; //Returns the balance variable to update it globally
        } else { //Else condition if the users balance is lower than the item price
            console.log('You did not have enough credit, your balance is ' + '£' + balance.toFixed(2) + ' and the item costs: ' + '£' + pepsi.price); //Tells the user they did not have enough balance and informs them of their balance and the items cost
            options(); //Calls the option function
        }
    } else if(select == coke.id) { //ElseIf for the next ID
        console.log("You have selected: " + coke.name); //Shows the user the item they have selected
        if(balance >= coke.price) {
            balance = balance - coke.price; //Removes the item price from the current balance and sets the balance variable to that number
            console.log('Thank you for your purchase' + "\n" +'Your new balance after the purchase is: ' + '£' + balance.toFixed(2) + "\n"); //Prints the new balance after the purchase
            options(); //Calls the option function
            return balance; //Returns the balance variable to update it globally
        } else { //Else condition if the users balance is lower than the item price
            console.log('You did not have enough credit, your balance is ' + '£' + balance.toFixed(2) + ' and the item costs: ' + '£' + coke.price); //Tells the user they did not have enough balance and informs them of their balance and the items cost
            options(); //Calls the option function
        }
    } else { //Else condition for when the user inputs an ID that does not match an of the product IDs 
        console.log('The ID you input did not match any of our products please check the ID again and try again'); //Tells the user they have input an incorrect ID
        listItems(); //Calls the listItems function
        selection(); //Calls the selection function
    } 
}

display(); //Calls the display function when the file is initialised
dateTimeToday(); //Calls the dateTimeToday function when the file is initialised
options(); //Calls the options function when the file is initialised